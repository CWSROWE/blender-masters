# Author: Stuart Rowe (stuart@rowe.rocks)
# Date: 26DEC2020

"""
Before you start:
- Blender:
    Edit \ Preferences \ File Paths -> set fonts folder. This is where the fonts you want to use will come from.
    Edit \ Preferences \ Addons -> ensure you have: 
        Add Mesh: Extra Objects
        Interface: Modifier Tools
    (if you followed the tutorial on Youtube then you should have this already)

Open Blender then go to the Scripting tab.
Click on Open and select this script.
At the bottom of the file, at line 629, you will see the font setting
 set font to SNOWBREAK
t.font_name = "LEMONMILK-Regular.ttf"

change the font name in the variable to the specific filename of your font. Case is ImPoRtAnT here!

Below this are the commands to create and save the various dice. 

d20 = t.gen_d20()
d12 = t.gen_d12()
d10 = t.gen_d10()
d1d = t.gen_d100()
d8 = t.gen_d8()
d6 = t.gen_d6()
d4 = t.gen_d4()
t.save(d20)
t.save(d12)
t.save(d10)
t.save(d1d)
t.save(d8)
t.save(d6)
t.save(d4)

Comment (put a # sign) in front of the ones you don't want to generate or save. 
If you dont want to generate a die, make sure you comment or delete the line where it tries to save that object,
otherwise you will get a lot of errors and dice after that point will not save.

Then, click on the Run Script in the top right of hte scripting window.
"""

import bpy
from enum import Enum
import mathutils
import math
import os
from glob import glob

class DiceMaker():
    fonts_list = []
    con = None
    font_name = None

    def __init__(self):
        self.fonts_list = self.load_fonts()
        self.con = bpy.context
        self.coll_scene = self.con.scene.collection
        self.coll_parents = self.parent_lookup(self.coll_scene)
        self.coll_target = self.coll_scene.children.get("Collection")
        self.active_coll = self.con.view_layer.active_layer_collection.collection
        self.active_coll_parent = self.coll_parents.get(self.active_coll.name)
        if self.active_coll_parent:
            self.active_coll_parent.children.unlink(self.active_coll)

    class Axis(Enum):
        X = 0
        Y = 1
        Z = 2

    def show_fonts(self):
        return self.fonts_list

    def load_fonts(self):
        output = []
        self.font_dir = bpy.context.preferences.filepaths.font_directory
        for f in glob(os.path.join(self.font_dir, "*.ttf")):
            bpy.ops.font.open(filepath = os.path.join(self.font_dir, f), relative_path=False)
            output.append(f.split('/')[-1])
        return output

    def get_font(self, name):
        if name in self.fonts_list:
            return bpy.data.fonts.load(os.path.join(bpy.context.preferences.filepaths.font_directory, name))
        
    def rot(self, obj, axis, value, keep_rotation=True):
        obj.select_set(True)
        bpy.context.object.rotation_euler[axis.value] = math.radians(value)
        if keep_rotation:
            bpy.ops.object.transform_apply(rotation=True)
        return "{'FINISHED'}"
        
    def resc(self, obj, x, y, z):
        obj.select_set(True)
        obj.scale[0] = x if x else obj.scale.x
        obj.scale[1] = y if y else obj.scale.y
        obj.scale[2] = z if z else obj.scale.z
        return "{'FINISHED'}"
        
    def mv(self, obj, x, y, z):
        obj.select_set(True)
        obj.location.x = x
        obj.location.y = y
        obj.location.z = z
        return "{'FINISHED'}"
        
    def traverse_tree(self, t):
        yield t
        for child in t.children:
            yield from self.traverse_tree(child)
        
    def parent_lookup(self, coll):
        parent_lookup = {}
        for coll in self.traverse_tree(coll):
            for c in coll.children.keys():
                parent_lookup.setdefault(c, coll)
        return parent_lookup

    def bool_diff(self, main_obj, diff_obj, move_when_done = True):
        obj = bpy.data.objects
        m = obj[main_obj.name]
        d = obj[diff_obj.name]
        m.select_set(True)
        bool = m.modifiers.new(type="BOOLEAN", name="bool-1")
        bool.object = d
        bool.operation = 'DIFFERENCE'
        if 'd10' in m.name:
            bpy.ops.object.apply_all_modifiers()
        else:
            bpy.ops.object.modifier_apply(apply_as='DATA', modifier=bool.name)
        if move_when_done:
            self.mv(d, -16, -16, d.location.z)

    def bool_intersect(self, main_obj, diff_obj):
        obj = bpy.data.objects
        m = obj[main_obj.name]
        d = obj[diff_obj.name]
        m.select_set(True)
        d.select_set(False)
        bool = m.modifiers.new(type="BOOLEAN", name="bool-2")
        bool.object = d
        bool.operation='INTERSECT'
        bpy.ops.object.apply_all_modifiers()
        bpy.data.objects.remove(d, do_unlink=True)

    def create_num(self, name, parent):
        text = bpy.data.curves.new(type="FONT", name=parent.name+"-"+name)
        text.body = name
        text.family = self.font_name.split('.')[0]
        text.font = self.get_font(self.font_name)
        if name == "6" or name == "9":
            text.body_format[0].use_underline = True
        obj = bpy.data.objects.new(parent.name+"-"+name, text)
        bpy.context.scene.collection.objects.link(obj)
        bpy.data.objects.get(parent.name).select_set(False)
        bpy.data.objects.get(obj.name).select_set(True)
        bpy.ops.object.origin_set(type='ORIGIN_CENTER_OF_MASS', center='BOUNDS')
        bpy.data.objects.get(parent.name).select_set(True)
        c = obj.data
        c.extrude = 2
        bpy.ops.object.convert(target='GPENCIL')
        bpy.ops.object.convert(target='MESH')
        return obj

    def create_d10_num(self, name, parent):
        text = bpy.data.curves.new(type="FONT", name=parent.name+"-"+name)
        text.body = name
        text.family = self.font_name.split('.')[0]
        text.font = self.get_font(self.font_name)
        if name == "6" or name == "9":
            text.body_format[0].use_underline = True
        obj = bpy.data.objects.new(parent.name+"-"+name, text)
        bpy.context.scene.collection.objects.link(obj)
        bpy.data.objects.get(obj.name).select_set(True)
        bpy.context.view_layer.objects.active = obj
        bpy.data.objects.get(parent.name).select_set(False)
        bpy.ops.object.origin_set(type='ORIGIN_CENTER_OF_MASS', center='BOUNDS')
        bpy.data.objects.get(parent.name).select_set(True)
        c = obj.data
        c.extrude = 2
        bpy.ops.object.convert(target='GPENCIL')
        bpy.ops.object.convert(target='MESH')
        return obj

    def create_d6(self):
        bpy.ops.mesh.primitive_cube_add(size=16.0)
        bpy.ops.object.origin_set(type='ORIGIN_CENTER_OF_MASS', center='BOUNDS')
        d = bpy.context.active_object
        d.name = "d6"
        d.select_set(True)
        return d

    def create_d4(self):
        bpy.ops.mesh.primitive_solid_add()
        d = bpy.context.active_object
        d.name = "d4"
        d.select_set(True)
        d.dimensions.z = 20.0
        _get_z_scale = d.scale.z
        d.scale.x = _get_z_scale
        d.scale.y = _get_z_scale
        bpy.ops.object.origin_set(type='ORIGIN_CENTER_OF_MASS', center='BOUNDS')
        self.rot(d, self.Axis.Z, -30)
        self.rot(d, self.Axis.Y, 180)
        return d

    def create_d8(self):
        bpy.ops.mesh.primitive_solid_add(source='8')
        d = bpy.context.active_object
        d.name = "d8"
        d.select_set(True)
        bpy.ops.object.origin_set(type='ORIGIN_CENTER_OF_MASS', center='BOUNDS')
        self.rot(d, self.Axis.Y, 45.0)
        self.rot(d, self.Axis.X, 35.265)
        d.dimensions.z = 16.4
        _get_z_scale = d.scale.z
        d.scale.x = _get_z_scale
        d.scale.y = _get_z_scale
        return d

    def create_d10(self, size=10):
        bpy.ops.mesh.primitive_cone_add(vertices=5, radius1=2, radius2=0, depth=2.25)
        d = bpy.context.active_object
        d.name = "d{}".format(size)
        d.select_set(True)    
        bpy.ops.object.duplicate()
        dd = bpy.context.active_object
        dd.name = "d{}a".format(size)
        d.select_set(False)
        dd.select_set(True)
        self.rot(dd, self.Axis.X, 180.0)
        dd.select_set(False)
        self.bool_intersect(d, dd)
        d.select_set(False)
        bpy.context.view_layer.objects.active = d
        bpy.ops.object.origin_set(type='ORIGIN_CENTER_OF_MASS', center='MEDIAN')
        d.select_set(True)
        self.rot(d, self.Axis.X, -54.3)
        d.dimensions.z = 17
        _zscale = d.scale.z
        d.scale.x = _zscale
        d.scale.y = _zscale
        return d

    def create_d12(self):
        bpy.ops.mesh.primitive_solid_add(source='12')
        d = bpy.context.active_object
        d.name = "d12"
        d.select_set(True)
        bpy.ops.object.origin_set(type="ORIGIN_CENTER_OF_MASS", center="BOUNDS")
        self.rot(d, self.Axis.X, -58.285)
        d.dimensions.z = 18.5
        _zscale = d.scale.z
        d.scale.x = _zscale
        d.scale.y = _zscale
        return d

    def create_d20(self):
        bpy.ops.mesh.primitive_solid_add(source='20')
        d = bpy.context.active_object
        d.name = "d20"
        d.select_set(True)
        bpy.ops.object.origin_set(type="ORIGIN_CENTER_OF_MASS", center="BOUNDS")
        self.rot(d, self.Axis.X, 20.905)
        d.dimensions.z = 20.7
        _zscale = d.scale.z
        d.scale.x = _zscale
        d.scale.y = _zscale
        return d
        
    def gen_d20(self): #certified
        d = self.create_d20()
        def do(num, obj):
            t = self.create_num(num, obj)
            self.resc(t, 5.2, 5.2, None)
            self.mv(t, 0.0, 0.0, 11)
            self.bool_diff(obj, t)
            t.select_set(False)
            bpy.data.objects.remove(t, do_unlink=True)
        do("20", d)
        self.rot(d, self.Axis.Z, 60.0)
        self.rot(d, self.Axis.X, 41.81)
        do("14", d)
        self.rot(d, self.Axis.Z, 60.0)
        self.rot(d, self.Axis.X, 41.81, False)
        do("6", d)
        self.rot(d, self.Axis.X, 0.0, False)
        self.rot(d, self.Axis.Z, -120.0)
        self.rot(d, self.Axis.X, 41.81, False)
        do("4", d)
        self.rot(d, self.Axis.X, 0.0, False)
        self.rot(d, self.Axis.Z, 60.0)
        self.rot(d, self.Axis.X, -41.81)
        self.rot(d, self.Axis.Z, 120.0)
        self.rot(d, self.Axis.X, 41.81)
        do("8", d)
        self.rot(d, self.Axis.Z, 60.0)
        self.rot(d, self.Axis.X, 41.81, False)
        do("10", d)
        self.rot(d, self.Axis.X, 0.0, False)
        self.rot(d, self.Axis.Z, -120.0)
        self.rot(d, self.Axis.X, 41.81, False)
        do("16", d)
        self.rot(d, self.Axis.X, 0.0, False)
        self.rot(d, self.Axis.Z, 60.0)
        self.rot(d, self.Axis.X, -41.81)
        self.rot(d, self.Axis.Z, 120.0)
        self.rot(d, self.Axis.X, 41.81)
        do("2", d)
        self.rot(d, self.Axis.Z, 60.0)
        self.rot(d, self.Axis.X, 41.81, False)
        do("18", d)
        self.rot(d, self.Axis.X, 0.0, False)
        self.rot(d, self.Axis.Z, -120.0)
        self.rot(d, self.Axis.X, 41.81, False)
        do("12", d)
        self.rot(d, self.Axis.X, 0.0, False)
        self.rot(d, self.Axis.Z, 60.0)
        self.rot(d, self.Axis.X, -41.81)
        self.rot(d, self.Axis.Z, 60)
        self.rot(d, self.Axis.X, 180.0)
        do("1", d)
        self.rot(d, self.Axis.Z, 60.0)
        self.rot(d, self.Axis.X, 41.81)
        do("19", d)
        self.rot(d, self.Axis.Z, 60.0)
        self.rot(d, self.Axis.X, 41.81, False)
        do("9", d)
        self.rot(d, self.Axis.X, 0.0, False)
        self.rot(d, self.Axis.Z, -120.0)
        self.rot(d, self.Axis.X, 41.81, False)
        do("3", d)
        self.rot(d, self.Axis.X, 0.0, False)
        self.rot(d, self.Axis.Z, 60.0)
        self.rot(d, self.Axis.X, -41.81)
        self.rot(d, self.Axis.Z, 120.0)
        self.rot(d, self.Axis.X, 41.81)
        do("13", d)
        self.rot(d, self.Axis.Z, 60.0)
        self.rot(d, self.Axis.X, 41.81, False)
        do("5", d)
        self.rot(d, self.Axis.X, 0.0, False)
        self.rot(d, self.Axis.Z, -120.0)
        self.rot(d, self.Axis.X, 41.81, False)
        do("11", d)
        self.rot(d, self.Axis.X, 0.0, False)
        self.rot(d, self.Axis.Z, 60.0)
        self.rot(d, self.Axis.X, -41.81)
        self.rot(d, self.Axis.Z, 120.0)
        self.rot(d, self.Axis.X, 41.81)
        do("7", d)
        self.rot(d, self.Axis.Z, 60.0)
        self.rot(d, self.Axis.X, 41.81, False)
        do("17", d)
        self.rot(d, self.Axis.X, 0.0, False)
        self.rot(d, self.Axis.Z, -120.0)
        self.rot(d, self.Axis.X, 41.81, False)
        do("15", d)
        self.rot(d, self.Axis.X, 0.0, False)
        self.rot(d, self.Axis.Z, 60.0)
        self.rot(d, self.Axis.X, -41.81)
        self.rot(d, self.Axis.Z, 120.0)
        self.rot(d, self.Axis.X, -41.81)
        self.rot(d, self.Axis.Z, 60)
        return d

    def gen_d12(self): #certified
        d = self.create_d12()
        def do(num, obj):
            t = self.create_num(num, obj)
            self.mv(t, 0.0, 0.0, 9.75)
            self.resc(t, 10.0, 10.0, None)
            self.bool_diff(obj, t)
            bpy.data.objects.remove(t, do_unlink=True)
        do("12", d)
        self.rot(d, self.Axis.Z, 36.0)
        self.rot(d, self.Axis.X, 63.43, False)
        do("8", d)
        self.rot(d, self.Axis.X, 0.0)
        self.rot(d, self.Axis.Z, 72.0)
        self.rot(d, self.Axis.X, 63.43, False)
        do("6", d)
        self.rot(d, self.Axis.X, 0.0)
        self.rot(d, self.Axis.Z, 72.0)
        self.rot(d, self.Axis.X, 63.43, False)
        do("4", d)
        self.rot(d, self.Axis.X, 0.0)
        self.rot(d, self.Axis.Z, 72.0)
        self.rot(d, self.Axis.X, 63.43, False)
        do("2", d)
        self.rot(d, self.Axis.X, 0.0)
        self.rot(d, self.Axis.Z, 72.0)
        self.rot(d, self.Axis.X, 63.43, False)
        do("10", d)
        self.rot(d, self.Axis.X, 0.0)
        self.rot(d, self.Axis.Z, 36.0)
        self.rot(d, self.Axis.X, 180.0)
        do("1", d)
        self.rot(d, self.Axis.Z, 36.0)
        self.rot(d, self.Axis.X, 63.43, False)
        do("3", d)
        self.rot(d, self.Axis.X, 0.0)
        self.rot(d, self.Axis.Z, 72.0)
        self.rot(d, self.Axis.X, 63.43, False)
        do("11", d)
        self.rot(d, self.Axis.X, 0.0)
        self.rot(d, self.Axis.Z, 72.0)
        self.rot(d, self.Axis.X, 63.43, False)
        do("9", d)
        self.rot(d, self.Axis.X, 0.0)
        self.rot(d, self.Axis.Z, 72.0)
        self.rot(d, self.Axis.X, 63.43, False)
        do("7", d)
        self.rot(d, self.Axis.X, 0.0)
        self.rot(d, self.Axis.Z, 72.0)
        self.rot(d, self.Axis.X, 63.43, False)
        do("5", d)
        return d

    def gen_d6(self): # certified
        d = self.create_d6()
        def do(num, obj):
            t = self.create_num(num, obj)
            self.resc(t, 12.50, 12.50, None)
            self.mv(t, 0, 0, 8.6)
            self.bool_diff(obj, t)
            t.select_set(False)
            bpy.data.objects.remove(t, do_unlink=True)
        do("6", d)
        self.rot(d, self.Axis.X, -90)
        do("2", d)
        self.rot(d, self.Axis.Y, 90)
        self.rot(d, self.Axis.Z, 180)
        do("4", d)
        self.rot(d, self.Axis.X, 180)
        do("3", d)
        self.rot(d, self.Axis.X, -90)
        self.rot(d, self.Axis.Z, 90)
        do("1", d)
        self.rot(d, self.Axis.X, 90)
        self.rot(d, self.Axis.Z, 180)
        do("5", d)
        self.rot(d, self.Axis.X, -90)
        self.rot(d, self.Axis.Z, -90)
        return d

    def gen_d10(self): #certified
        d = self.create_d10()
        def do(num, obj):
            t = self.create_d10_num(num, obj)
            self.resc(t, 7.5, 7.5, None)
            self.mv(t, 0, -0.75, 9)
            self.bool_diff(obj, t, True)
            t.select_set(False)
            bpy.data.objects.remove(t, do_unlink=True)
            bpy.context.view_layer.objects.active = obj
        do("6", d)
        self.rot(d, self.Axis.X, -35.7)
        self.rot(d, self.Axis.Y, -72.0)
        self.rot(d, self.Axis.X, 35.7)
        do("4", d)
        self.rot(d, self.Axis.X, -35.7)
        self.rot(d, self.Axis.Y, -72.0)
        self.rot(d, self.Axis.X, 35.7)
        do("8", d)
        self.rot(d, self.Axis.X, -35.7)
        self.rot(d, self.Axis.Y, -72.0)
        self.rot(d, self.Axis.X, 35.7)
        do("2", d)
        self.rot(d, self.Axis.X, -35.7)
        self.rot(d, self.Axis.Y, -72.0)
        self.rot(d, self.Axis.X, 35.7)
        do("0", d)
        self.rot(d, self.Axis.X, 180.0)
        do("1", d)
        self.rot(d, self.Axis.X, -35.7)
        self.rot(d, self.Axis.Y, -72.0)
        self.rot(d, self.Axis.X, 35.7)
        do("9", d)
        self.rot(d, self.Axis.X, -35.7)
        self.rot(d, self.Axis.Y, -72.0)
        self.rot(d, self.Axis.X, 35.7)
        do("3", d)
        self.rot(d, self.Axis.X, -35.7)
        self.rot(d, self.Axis.Y, -72.0)
        self.rot(d, self.Axis.X, 35.7)
        do("7", d)
        self.rot(d, self.Axis.X, -35.7)
        self.rot(d, self.Axis.Y, -72.0)
        self.rot(d, self.Axis.X, 35.7)
        do("5", d)
        self.rot(d, self.Axis.X, -35.7)
        self.rot(d, self.Axis.Y, -72.0)
        self.rot(d, self.Axis.X, 35.7)
        return d

    def gen_d100(self): #certified
        d = self.create_d10(size=100)
        def do(num, obj):
            t = self.create_num(num, obj)
            self.resc(t, 7.5, 7.5, None)
            self.mv(t, 0, -0.25, 9)
            self.bool_diff(obj, t, True)
            bpy.data.objects.remove(t, do_unlink=True)        
        do("60", d)
        self.rot(d, self.Axis.X, -35.7)
        self.rot(d, self.Axis.Y, -72.0)
        self.rot(d, self.Axis.X, 35.7)
        do("40", d)
        self.rot(d, self.Axis.X, -35.7)
        self.rot(d, self.Axis.Y, -72.0)
        self.rot(d, self.Axis.X, 35.7)
        do("80", d)
        self.rot(d, self.Axis.X, -35.7)
        self.rot(d, self.Axis.Y, -72.0)
        self.rot(d, self.Axis.X, 35.7)
        do("20", d)
        self.rot(d, self.Axis.X, -35.7)
        self.rot(d, self.Axis.Y, -72.0)
        self.rot(d, self.Axis.X, 35.7)
        do("00", d)
        self.rot(d, self.Axis.X, 180.0)
        do("10", d)
        self.rot(d, self.Axis.X, -35.7)
        self.rot(d, self.Axis.Y, -72.0)
        self.rot(d, self.Axis.X, 35.7)
        do("90", d)
        self.rot(d, self.Axis.X, -35.7)
        self.rot(d, self.Axis.Y, -72.0)
        self.rot(d, self.Axis.X, 35.7)
        do("30", d)
        self.rot(d, self.Axis.X, -35.7)
        self.rot(d, self.Axis.Y, -72.0)
        self.rot(d, self.Axis.X, 35.7)
        do("70", d)
        self.rot(d, self.Axis.X, -35.7)
        self.rot(d, self.Axis.Y, -72.0)
        self.rot(d, self.Axis.X, 35.7)
        do("50", d)
        self.rot(d, self.Axis.X, -35.7)
        self.rot(d, self.Axis.Y, -72.0)
        self.rot(d, self.Axis.X, 35.7)
        return d

    def gen_d4(self): # certified
        d = self.create_d4()
        def do(num, obj):
            t = self.create_num(num, obj)
            self.resc(t, 8.0, 8.0, None)
            self.mv(t, 0, 6, 5.6)
            self.bool_diff(obj, t, False)
            t.select_set(False)
            self.rot(obj, self.Axis.X, -19.47)
            self.rot(obj, self.Axis.Y, 120)
            self.rot(obj, self.Axis.X, 19.47)
            self.bool_diff(obj, t, False)
            self.rot(obj, self.Axis.X, -19.47)
            self.rot(obj, self.Axis.Y, 120)
            self.rot(obj, self.Axis.X, 19.47)
            self.bool_diff(obj, t, True)
            bpy.data.objects.remove(t, do_unlink=True)
        do("4", d)
        self.rot(d, self.Axis.Z, 120)
        do("3", d)
        self.rot(d, self.Axis.Z, -120)
        do("2", d)
        self.rot(d, self.Axis.X, -19.47)
        self.rot(d, self.Axis.Y, 120)
        self.rot(d, self.Axis.X, 19.47)
        self.rot(d, self.Axis.Z, -120)
        do("1", d)
        self.rot(d, self.Axis.X, -19.47)
        self.rot(d, self.Axis.Y, 120)
        self.rot(d, self.Axis.X, 19.47)
        return d

    def gen_d8(self): #certified
        d = self.create_d8()
        def do(num, obj):
            t = self.create_num(num, obj)
            self.resc(t, 10.0, 10.0, None)
            self.mv(t, 0, 0, 8.6)
            self.bool_diff(obj, t, True)
            bpy.data.objects.remove(t, do_unlink=True)
        do("8", d)
        self.rot(d, self.Axis.X, -35.265)
        self.rot(d, self.Axis.Y, -90)
        self.rot(d, self.Axis.X, 35.265)
        do("2", d)
        self.rot(d, self.Axis.X, -35.265)
        self.rot(d, self.Axis.Y, -90)
        self.rot(d, self.Axis.X, 35.265)
        do("6", d)
        self.rot(d, self.Axis.X, -35.265)
        self.rot(d, self.Axis.Y, -90)
        self.rot(d, self.Axis.X, 35.265)
        do("4", d)
        self.rot(d, self.Axis.X, 180.0)
        do("5", d)
        self.rot(d, self.Axis.X, -35.265)
        self.rot(d, self.Axis.Y, -90)
        self.rot(d, self.Axis.X, 35.265)
        do("3", d)
        self.rot(d, self.Axis.X, -35.265)
        self.rot(d, self.Axis.Y, -90)
        self.rot(d, self.Axis.X, 35.265)
        do("7", d)
        self.rot(d, self.Axis.X, -35.265)
        self.rot(d, self.Axis.Y, -90)
        self.rot(d, self.Axis.X, 35.265)
        do("1", d)
        return d

    def save(self, obj):
        bpy.ops.object.select_all(action='DESELECT')
        path = bpy.context.preferences.filepaths.font_directory
        obj.select_set(True)
        stl_path = path+f"{obj.name}.stl"
        bpy.ops.export_mesh.stl(filepath=str(stl_path),use_selection=True)
        obj.select_set(False)



# instantiate class
t = DiceMaker()
# set font to SNOWBREAK
t.font_name = "LEMONMILK-Regular.ttf"
d20 = t.gen_d20()
d12 = t.gen_d12()
d10 = t.gen_d10()
d1d = t.gen_d100()
d8 = t.gen_d8()
d6 = t.gen_d6()
d4 = t.gen_d4()
t.save(d20)
t.save(d12)
t.save(d10)
t.save(d1d)
t.save(d8)
t.save(d6)
t.save(d4)