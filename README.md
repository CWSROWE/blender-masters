# blender-masters #

Repo for blender-masters, a python script to create dice masters in Blender with custom fonts.
Dice maths from this amazing video: https://www.youtube.com/watch?v=nCowrvfOr3Q

### Requirements ###

* Blender (tested in v2.81.16)
* with the following addons enabled:
* * Add Mesh: Extra Objects
* * Interface: Modifier Tools
* and your fonts (ttf format) in a directory which is then set in Edit - Preferences - FilePaths

### How do I run it? ###
This is an older video: https://www.youtube.com/watch?v=E5mXNxoI2cU
